# devops

Training project

# terraform gitignore

**согласно файлу terraform/.gitignore будут проигнорированы файлы:**

* `**/.terraform/*`  все содержимое в директориях с названием .terraform
* `*.tfstate`        все файлы, с именами, заканчивающимися на .tfstate
* `*.tfstate.*`	 все файлы, содержащие в имени .tfstate.
* `crash.log`	 файлы с названием crash.log
* `*.tfvars`	 все файлы, имена которых заканчиваются на .tfvars
* `override.tf`	 файлы с названием override.tf
* `override.tf.json` файлы с названием override.tf.json
* `*_override.tf`    файлы, имена которых заканчиваются на _override.tf
* `*_override.tf.json`  файлы, имена которых заканчиваются на _override.tf.json
* `.terraformrc`	 файлы с названием .terraformrc
* `terraform.rc`	 файлы с названием terraform.rc